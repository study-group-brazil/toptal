# Objetivos

### Aprender
Nosso primeiro objetivo e aprender. Seja para um processo de selecao ou para melhorar as nossas skills em determinadas areas.

### Ensinar
O processo de aprendizagem compreende muitas coisas, e uma delas e o ensino. Quer uma forma melhor de dominar que nao seja ensinando? Alem do mais a gente sempre sabe uma coisinha ou outra que outras pessoas nao sabem que podemos ensinar e aprender com elas.

### Compartilhar
Todo o conhecimento aqui adquirido sera repassado a todos de forma aberta e clara. Podemos usar blog posts, videos e lives para compartilhar o que estamos aprendendo ate agora.

# Como fazer?
### Conteudo
Abaixo segue uma lista do que achamos essensial. 

### Reunioes
A definir. Possivelmente um link do appear.in toda semana.

### Treinando
Para treinar o que foi aprendido, sugerimos seguir o plano de estudo do HackerRank e Codility:
* https://codility.com/programmers/lessons/
* https://www.hackerrank.com/domains/algorithms/warmup

# Conteudo
### Cursos
* https://www.coursera.org/specializations/algorithms
* https://www.udacity.com/course/intro-to-algorithms--cs215
* https://www.youtube.com/watch?v=V6mKVRU1evU&amp;list=PLGLfVvz_LVvReUrWr94U-ZMgjYTQ538nT&amp;index=9
* https://www.youtube.com/watch?v=iOq5kSKqeR4&list=PLuShgXhbmcNWZeypGD9dP2tbv4Z9XasVo
* https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-introduction-to-algorithms-sma-5503-fall-2005/index.htm
* http://aduni.org/courses/algorithms/index.php?view=cw
* https://www.edx.org/course/algorithms-iitbombayx-cs213-3x

### Good to know stuff
* http://bigocheatsheet.com/
* https://www.toptal.com/algorithms/interview-questions
* https://github.com/open-source-society/computer-science
